import React from 'react'
import Jumbotron from 'react-bootstrap/Jumbotron'
import {Container,Row ,Col} from 'react-bootstrap'
import axios from 'axios'
class Employee extends React.Component{

    constructor(props){
        super(props)
        this.name = React.createRef();
        this.uname = React.createRef();
        this.password = React.createRef();
        this.email = React.createRef();
        this.phone = React.createRef();

        this.state= {
                showError:false
        }
    }


    onSubmit(event){
        event.preventDefault();
        let name=this.name.current.value;
        let username=this.uname.current.value;
        let password=this.password.current.value;
        let email=this.email.current.value;
        let phone=this.phone.current.value;
        if(name!="" && username!="" && password!="" && email!="" && phone != ""){
            console.log(this.name.current.value)
            axios.post("http://3.219.183.226:8080/saveEmployee", {
                "emailId": email,
                
                "mobileNo": phone,
                "name": name,
                "password": password,
                "username": username
            }).then((response)=>{console.log(response)})

        }else {
            alert("Input data is wrong ! ")
        }
    }

    render(){

        return (
            <div>
                <Jumbotron>
                    <h3> Add Employee</h3> <hr/>

<form onSubmit={ (event)=> {this.onSubmit(event)}}>
  <div className="form-group row">
    <label >Name</label>
    <input type="text" className="form-control" ref={this.name}  aria-describedby="emailHelp" placeholder="Name" />
  </div>
  <div className="form-group row">
    <label >User Name</label>
    <input type="text" className="form-control" ref={this.uname} placeholder="User Name"/>
  </div>
  <div className="form-group row">
    <label >Password</label>
    <input type="password" className="form-control" ref={this.password} placeholder="Password"/>
  </div>

  <div className="form-group row">
    <label >Email ID</label>
    <input type="email" className="form-control" ref={this.email} placeholder="Email Id"/>
  </div>

  <div className="form-group row">
    <label >Phone Number</label>
    <input type="text" className="form-control" ref={this.phone} placeholder="123.."/>
  </div>


  <button type="submit" className="btn btn-primary">Submit</button>
</form>
</Jumbotron>
             


  
                
            </div>
        )
    }



}

export default Employee