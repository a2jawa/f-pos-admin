import React from 'react'
import Jumbotron from 'react-bootstrap/Jumbotron'
import {Container,Row ,Col} from 'react-bootstrap'
import axios from 'axios'
class Product extends React.Component{

    constructor(props){
        super(props)
        this.state= {

        }
        this.name = React.createRef()
        this.description = React.createRef()
        this.price = React.createRef()
        this.code = React.createRef()
        this.stock = React.createRef()
    }
//"http://54.90.166.158
    onSubmit(event){
        event.preventDefault();
        let name=this.name.current.value;
        let description=this.description.current.value;
        let price=this.price.current.value;
        let code=this.code.current.value;
        let stock=this.stock.current.value;
        if(name!="" && description!="" && price!=""  && stock != ""){
            axios.post("http://3.219.183.226:8080/saveProduct", {
            "description": description,
            "name": name,
            "price": price,
            "productCode": code,
            "stock": stock
        }
            ).then((response)=>{console.log(response)})
        }
    console.log("done")}


    render(){

        return (
            <div>
                <Jumbotron>
                <h3> Add Product</h3> <hr/>

<form onSubmit={ (event)=> {this.onSubmit(event)}}>
  <div className="form-group row">
    <label >Name</label>
    <input type="text" className="form-control"  ref={this.name} aria-describedby="emailHelp" placeholder="Name"/>
  </div>
  <div className="form-group row">
    <label >Description</label>
    <input type="text" className="form-control" ref={this.description} placeholder="Description"/>
  </div>
  <div className="form-group row">
    <label >Price</label>
    <input type="text" className="form-control" ref={this.price} placeholder="Price"/>
  </div>

  <div className="form-group row">
    <label >Code</label>
    <input type="text" className="form-control" ref={this.code} placeholder="Product Code"/>
  </div>

  <div className="form-group row">
    <label >Stock</label>
    <input type="text" className="form-control" ref={this.stock} placeholder="stock"/>
  </div>


  <button type="submit" className="btn btn-primary">Submit</button>
</form>
</Jumbotron>
             


  
                
            </div>
        )
    }



}

export default Product