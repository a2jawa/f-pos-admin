import React from 'react';
import logo from './logo.svg';
import './App.css';
import Employee from './Components/Employee/employee.js'
import Product from './Components/Products/products.js'
import {Container,Row ,Col} from 'react-bootstrap'
import Sky from 'react-sky';



function App() {
  return (
    <div className="App">
       <Sky
          images={{
            0: "https://image.flaticon.com/icons/svg/135/135728.svg",
            1: "https://image.flaticon.com/icons/svg/135/135687.svg",
            2: "https://image.flaticon.com/icons/svg/135/135715.svg",
            3: "https://image.flaticon.com/icons/svg/135/135628.svg",
            4: "https://image.flaticon.com/icons/svg/135/135591.svg",
            5: "https://image.flaticon.com/icons/svg/135/135702.svg",
            6: "https://image.flaticon.com/icons/svg/135/135644.svg",
            7: "https://image.flaticon.com/icons/svg/135/135629.svg",
            8: "https://image.flaticon.com/icons/svg/135/135630.svg" 
          }}
          how={130} /* Pass the number of images Sky will render chosing randomly */
          time={40} /* time of animation */
          size={'100px'} /* size of the rendered images */
          background={'palettedvioletred'} /* color of background */
        />
      <br/>
      <br/>

      <Container>
     
        <Row>
          <Col>
           <Employee/>
          </Col>
          <Col>
           <Product/>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default App;
